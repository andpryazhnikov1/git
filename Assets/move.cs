using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class move : MonoBehaviour
{
    public Image groupButton;
    public Canvas screen;
    public string napravlenie;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (napravlenie)
        {
            case "up":
                if (groupButton.transform.position.y < screen.transform.position.y + 84.0f)
                    groupButton.transform.position += new Vector3(0, 0.1f, 0);
                break;
            case "down":
                if (groupButton.transform.position.y > screen.transform.position.y - 78.0f)
                groupButton.transform.position += new Vector3(0, -0.1f, 0);
                break;
            case "right":
                if (groupButton.transform.position.x < screen.transform.position.x + 295.0f)
                groupButton.transform.position += new Vector3(0.1f, 0, 0);
                break;
            case "left":
            if (groupButton.transform.position.x > screen.transform.position.x - 295.0f)
                groupButton.transform.position += new Vector3(-0.1f, 0, 0);
                break;
        }
    }

    public void Move(string X)
    {
        switch (X)
        {
            case "up":
                napravlenie = X;
                break;
            case "down":
                napravlenie = X;
                break;
            case "right":
                napravlenie = X;
                break;
            case "left":
                napravlenie = X;
                break;
        }
    }
}
